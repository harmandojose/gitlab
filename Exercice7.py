a = input(str("Entrez un mot")).lower()
Premier=a[0]
Dernier=a[-1]

if Premier < Dernier:
    print("La lettre ",Premier, "apparait avant la lettre ",Dernier, "dans l'alphabet")
    
elif Premier == Dernier:
    print("Les lettres sont dans la même position dans l'alphabet")
    
else:
    print("La lettre ",Premier, "apparait après la lettre ",Dernier, "dans l'alphabet")
